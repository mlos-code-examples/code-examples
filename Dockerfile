FROM python:3.8-slim-buster

COPY requirements.txt .
RUN pip install -r requirements.txt
RUN apt-get update
RUN apt-get install -y curl
COPY . .
# ENTRYPOINT ["tail"]
# CMD ["-f","/dev/null"]
CMD ["timeout 900"]
ENTRYPOINT ["python"]
CMD ["-c", "print('specify test command')"]
