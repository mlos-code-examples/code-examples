
import os
from distutils import util
from typing import Dict, Union, List

import yaml
from deepmerge import always_merger


class ConfigLoader:
    ENV_CONFIG_KEY_SEPARATOR = '.'

    def __init__(
            self,
            dir_path: str,
            dist_config_filename: str = 'config.dist.yml',
            custom_config_filename='config.yml',
            env_prefix: Union[str, None] = 'CAT.'):
        self.dir_path = dir_path
        self.dist_config_filename = dist_config_filename
        self.user_config_filename = custom_config_filename
        self.env_prefix = env_prefix.upper()

    @classmethod
    def _parse_str_config(cls, value: str):
        try:
            return bool(util.strtobool(value))
        except ValueError:
            return value

    @classmethod
    def _get_env_var_by_prefix(cls, name_prefix: str) -> Dict:
        prefixed_env = {}
        for name, value in os.environ.items():
            if name.startswith(name_prefix.upper()):
                prefixed_env[name] = cls._parse_str_config(value)
        return prefixed_env

    @classmethod
    def _update_dict_by_path(cls, data: Dict, nested_keys: List[str], value) -> Dict:
        last_key = nested_keys.pop()
        data_node = data
        for key in nested_keys:
            data_node.setdefault(key, {})
            data_node = data_node[key]
        data_node[last_key] = value
        return data

    @classmethod
    def remove_prefix(cls, text, prefix):
        return text[len(prefix):] if text.startswith(prefix) else text

    @classmethod
    def _load_env_vars(cls, name_prefix: str, keys_separator: str = '.', force_lower_case: bool = True) -> Dict:
        config = {}
        env_vars = cls._get_env_var_by_prefix(name_prefix)
        for var_name, var_value in env_vars.items():
            var_name = cls.remove_prefix(var_name.upper(), name_prefix)
            config_nested_keys = var_name.split(keys_separator)
            if force_lower_case:
                config_nested_keys = [key.lower() for key in config_nested_keys]
            config = cls._update_dict_by_path(config, config_nested_keys, var_value)
        return config

    @classmethod
    def _load_yaml(cls, path) -> [Dict, None]:
        if os.path.exists(path):
            with open(path, 'r') as file:
                return yaml.safe_load(file)
        else:
            return None

    def get_config(self) -> dict:
        dist_config_path = os.path.join(self.dir_path, self.dist_config_filename)
        user_config_path = os.path.join(self.dir_path, self.user_config_filename)
        dist_config = self._load_yaml(dist_config_path) or {}
        user_config = self._load_yaml(user_config_path) or {}
        env_config = {}

        if self.env_prefix is not None:
            env_config = self._load_env_vars(
                self.env_prefix,
                keys_separator=self.ENV_CONFIG_KEY_SEPARATOR,
                force_lower_case=True)

        if user_config or dist_config or env_config:
            config = always_merger.merge(dist_config,user_config,)
            config = always_merger.merge(config, env_config)
            return config
        else:
            raise Exception('not found any of config files %s or system environments'
                            % ([dist_config_path, user_config_path]))
