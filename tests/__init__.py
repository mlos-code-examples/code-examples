import os

TESTS_PATH = os.path.dirname(os.path.abspath(__file__))
TESTS_OUTPUT_PATH = os.path.join(TESTS_PATH, '_output')

PROJECT_PATH = os.path.dirname(TESTS_PATH)
