from datetime import datetime
from time import sleep
from typing import Dict, Callable


class Tester:
    def __init__(self, config: Dict = None):
        self.config = config

    def use(self, use_case):  # deprecated
        return use_case(self)

    def do(self, use_case):
        return use_case(self)
