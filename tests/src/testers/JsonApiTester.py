from typing import Dict, List, Union

from lib.helpers.AssertHelper import AssertHelper
from lib.helpers.DebugHelper import DebugHelper
from tests.src.testers.HttpTester import HttpTester


class JsonApiTester(HttpTester):
    def _grab_last_json(self):
        last_response = self.grab_last_response()
        if last_response.content:
            return last_response.json()
        else:
            return None

    def send_get(self, path: str, params: Dict = None, **kwargs_params):
        HttpTester.send_get(self, path, params, **kwargs_params)
        return self._grab_last_json()

    def send_post(self, path: str, json: (Dict, str, list) = None, data: (Dict, str, list) = None, params: Dict = None,
                  **kwargs):
        HttpTester.send_post(self, path, json, data, params, **kwargs)
        return self._grab_last_json()

    def send_patch(self, path: str, json: (Dict, str, list) = None, data: (Dict, str, list) = None):
        HttpTester.send_patch(self, path, json, data)
        return self._grab_last_json()

    def send_put(self, path: str, params: Dict = None, json: (Dict, str, list) = None,
                 data: (Dict, str, list) = None):
        HttpTester.send_put(self, path, params, json, data)
        return self._grab_last_json()

    def send_delete(self, path: str, params: Dict = None, json: (Dict, str, list) = None,
                    data: (Dict, str, list) = None):
        HttpTester.send_delete(self, path, params, json, data)
        return self._grab_last_json()

    def grab_json(self) -> Union[Dict, List]:
        return self._grab_last_json()

    def see_json(self, json_data: Union[Dict, List]):
        assert \
            json_data == self._grab_last_json(), \
            'Unexpected JSON response. Expected "%s".\n%s' % (
                json_data,
                DebugHelper.get_printable_response(self._last_response)
            )

    def see_json_contains(self, json_data: Union[Dict, List]):
        assert \
            AssertHelper.is_contains(json_data, self._grab_last_json()) is True, \
            'Not found expected JSON in response. Expected "%s".\n%s' % (
                json_data,
                DebugHelper.get_printable_response(self._last_response)
            )
