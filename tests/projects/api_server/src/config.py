import json
import xml
from dataclasses import dataclass


@dataclass
class RequestResponse:
    response: json


@dataclass
class XmlRequestResponse:
    response: xml


class Config:
    @classmethod
    def endpoints(cls):
        return {
            "user_1": r'/users/1',
            "user_2": r'/users/2',
            "user_3": r'/users/3',
            "post_create": r'/create'
        }

    @classmethod
    def response_get_user_1(cls):
        return RequestResponse(
            response={
                "name": "Andrew",
                "userId": 1,
                "status": "alive"
            }
        )

    @classmethod
    def response_get_user_2(cls):
        return RequestResponse(
            response={
                "name": "Tom",
                "userId": 2,
                "status": "dead"
            }
        )

    @classmethod
    def response_post(cls):
        return RequestResponse(
            response={
                "operation_status": "success",
                "data": {
                    "name": "test",
                    "status": "alive",
                    "id": 25
                }
            }
        )