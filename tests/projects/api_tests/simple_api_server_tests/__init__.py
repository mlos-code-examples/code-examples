import os

from lib.helpers.ConfigLoader import ConfigLoader

path = os.path.dirname(__file__)

SIMPLE_API_SERVER_CONFIG = ConfigLoader(path).get_config()['simple_api_server']
