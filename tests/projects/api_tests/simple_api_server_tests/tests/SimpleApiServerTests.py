
from unittest import TestCase
from tests.projects.api_tests.simple_api_server_tests.src.SimpleApiTester import SimpleApiTester


class SimpleApiServerTests(TestCase):
    def setUp(self) -> None:
        self.tester = SimpleApiTester()

    def test_get_user(self):
        # given
        user_id = 1
        name = 'Andrew'
        status = 'alive'

        # when
        self.tester.get_user(user_id)

        # then
        self.tester.see_status_code(200)
        self.tester.see_user(name, status)

    def test_get_not_existing_user(self):
        # given
        user_id = 2

        # when
        self.tester.get_user(user_id)

        # then
        self.tester.see_status_code(404)

    def test_add_new_user(self):
        # given
        name = "test"
        status = "alive"

        # when
        self.tester.create_user(name, status)

        # then
        self.tester.see_created_user(operation_status="success", name=name, status=status)



