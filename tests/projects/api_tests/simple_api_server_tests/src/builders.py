from random import randint


class RequestDataBuilder:
    def __init__(self):
        self.external_operation_id: int = randint(0, 999999)

    def build(self):
        return {
            "externalOperationId": self.external_operation_id
        }
