from unittest import TestCase
from tests.projects.api_tests.simple_api_server_tests.src.SimpleApiTester import SimpleApiTester


class SimpleApiTests(TestCase):
    def setUp(self) -> None:
        self.tester: SimpleApiTester = SimpleApiTester()
