from tests.projects.api_tests.simple_api_server_tests import SIMPLE_API_SERVER_CONFIG
from tests.src.testers.JsonApiTester import JsonApiTester


class SimpleApiTester(JsonApiTester):
    def __init__(self, config=SIMPLE_API_SERVER_CONFIG, *args, **kwargs):
        super().__init__(config=config, *args, **kwargs)

    def get_user(self, user_id: int):
        return self.send_get(f'users/{user_id}')

    def create_user(self, name: str, status: str):
        data = {
            "name": name,
            "status": status
        }
        return self.send_post('create/', json=data)

    def see_user(self, name: str, status: str = "alive"):
        self.see_json_contains(
            {
                "name": name,
                "userId": 1,
                "status": status
            }
        )

    def see_created_user(self, operation_status: str, name: str, status: str):
        self.see_json_contains(
            {
                "operation_status": operation_status,
                "data": {
                    "name": name,
                    "status": status,
                    "id": 25
                }
            }
        )
