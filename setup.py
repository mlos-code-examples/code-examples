import setuptools

setuptools.setup(
    name='automatic-tests',
    version='1.0',
    packages=setuptools.find_packages(),
    python_requires='>=3.7'
)
